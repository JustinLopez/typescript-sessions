// 1. Números y operaciones matemáticas: Crea una función que tome dos números como parámetros y devuelva su suma.

const sumNumbers = (firstNumber: number, secondNumber: number): number => firstNumber + secondNumber;

const result = sumNumbers(12, 10);

console.log(result); 
