// 10. Crea una función externa que reciba un número y devuelva una función interna que multiplique ese número por un valor dado.

const createMultiply = (value: number) => {
    return (number: number): number => {
        return number * value;
    };
};

const multiplyBy = createMultiply(5);
console.log(multiplyBy(8));
console.log(multiplyBy(32)); 
