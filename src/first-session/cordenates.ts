// 5. Define una tupla que represente las coordenadas (x, y) de un punto y crea una función que reciba esta tupla y devuelva la distancia desde el origen (0, 0).

const distanceFromOrigin = (x: number, y: number): number => Math.sqrt(x ** 2 + y ** 2);

const x = 3;
const y = 4;
console.log(distanceFromOrigin(x, y));
