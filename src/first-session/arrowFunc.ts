// 9. Convierte una función normal que calcule el cuadrado de un número en una función flecha.

const calculateSquare = (number: number): number => number * number;
