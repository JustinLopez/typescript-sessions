// 3. Implementa una función que determine si un número es par o impar y devuelva un booleano.

const isPair = (number: number): boolean => number % 2 === 0;


console.log(isPair(41)); 
console.log(isPair(28));
