// 4, Crea una función que tome un array de números y devuelva la suma de todos los elementos.

const sumArray = (numbers: number[]): number => numbers.reduce((accumulator , number) => accumulator  + number, 0);

const numbers = [10, 20, 10, 10, 20];

console.log(sumArray(numbers));
