// 2. Escribe una función que tome dos cadenas y las concatene en una sola cadena.

const concatenateStrings = (firstString: string, secondString: string): string => `${firstString} ${secondString}`;

const concatenatedStrings = concatenateStrings("un", "smash? ☺");
console.log(concatenatedStrings); 
