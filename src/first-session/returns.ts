// 8. Tipado de parámetros y retorno: Implementa una función que tome un nombre (cadena) y una edad (número) como parámetros y devuelva un objeto con estos datos.

const createPerson = (name: string, age: number): { name: string, age: number } => {
    return { name, age };
}
const person = createPerson("Leonardo", 36);
console.log(person);
