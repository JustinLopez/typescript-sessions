// 6. Uso de Any: Escribe una función que tome un parámetro de tipo any y devuelva su tipo como cadena.

const getAsString = (parameter: any): string => {
    return typeof parameter;
}

console.log(getAsString(5)); 
console.log(getAsString("helloooo!"));
console.log(getAsString(true)); 
