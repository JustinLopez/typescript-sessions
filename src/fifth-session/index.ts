import fs from 'fs';

//decoradores

function validateLength(target: any, propertyKey: string) {
    let value = target[propertyKey];

    const getter = function () {
        return value;
    };

    const setter = function (newVal: string) {
        if (!newVal || newVal.length < 3) {
            throw new Error(`Name is required and should have at least 3 characters.`);
        }
        value = newVal;
    };

    Object.defineProperty(target, propertyKey, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true,
    });
}

// 1.1 Employee (con propiedades como name, age, role, etc.).

class Employee {
    @validateLength
    name: string;

    id: number;
    age: number;
    role: string;
    salary: number;

    constructor(id: number, name: string, age: number, role: string, salary: number) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.role = role;
        this.salary = salary;
    }
}


// 1.2 Manager (extiende de Employee e incluye propiedades adicionales como department).
class Manager extends Employee {
    department: string;

    constructor(id: number, name: string, age: number, role: string, salary: number, department: string) {
        super(id, name, age, role, salary);
        this.department = department;
    }
}
class QAEngineer extends Employee {
    technology: string;
    level: "Junior" | "Intermediate" | "Senior";

    constructor(id: number, name: string, age: number, role: string, salary: number, technology: string, level: "Junior" | "Intermediate" | "Senior") {
        super(id, name, age, role, salary);
        this.technology = technology;
        this.level = level;
    }
}

// 1.3 Intern (extiende de Employee e incluye propiedades específicas de pasantes).

interface Intern extends Employee {
    internshipDuration: "3 months" | "6 months" | "8 months";
    lead: string
};

interface QAEngineer extends Employee  {
    technology: string;
    level: "Junior" | "Intermediate" | "Senior";
};


//  2.1 Utiliza Union Types para representar diferentes roles dentro de la organización (por ejemplo, Manager | Intern).

type EmployeeRole = Manager | QAEngineer | Intern;

// 2.3 Emplea Intersection Types para definir tipos combinados que contengan propiedades de varios tipos (por ejemplo, Employee & { salary: number }).

type EmployeeExperience = Employee & { yearsOfExperience: number };
type EmployeeSalary = Employee & { salary: number };

// 3.1 Implementa tipos condicionales para definir tipos basados en condiciones lógicas (por ejemplo, un tipo que depende del valor de una propiedad).

type Transaction = {
    id: number;
    amount: number;
    bank: "Bac" | "Lafise" | "Banpro";
};

type BankMethod<T extends Transaction["bank"]> = T extends "Bac"
    ? { method: "Bac" }
    : T extends "Lafise"
    ? { method: "Lafise" }
    : { method: "Banpro" };

const testTransaction: Transaction = {
    id: 1,
    amount: 100,
    bank: "Bac"
};

const bankMethod1: BankMethod<typeof testTransaction["bank"]> = { method: "Bac" };
console.log(bankMethod1);

//  3.2 Utiliza tipos inferidos para  inferir tipos automáticamente en función de los valores asignados (por ejemplo, inferir el tipo de un empleado según su rol).

const createEmployee = (employee: Employee): Manager | Intern | QAEngineer => {
    switch (employee.role) {
        case "Manager":
            return employee as Manager;
        case "Intern":
            return employee as Intern;
        case "QA Engineer":
            return employee as QAEngineer;
        default:
            throw new Error("Unknown employee role");
    }
};

// 4.1 Emplea tipos mapeados para crear tipos nuevos a partir de tipos existentes (por ejemplo, para agregar o modificar propiedades en un tipo).

type AddProperty<T, K extends string, V> = {
    [P in keyof T | K]: P extends keyof T ? T[P] : V;
};

type EmployeeWithVacationAvailability = AddProperty<Employee, "vacationAvailability", boolean>;

const employeeWithBonus: EmployeeWithVacationAvailability = {
    id: 1,
    name: "John Doe",
    age: 30,
    role: "Manager",
    salary: 100,
    vacationAvailability: true
};

console.log('This an employee with bonus ->', employeeWithBonus);



// Cargar datos de empleados 

const jsonFilePath = 'employees.json';

class EmployeeManager {
    private employees: Employee[] = [];
    private jsonFilePath: string;

    constructor(jsonFilePath: string) {
        this.jsonFilePath = jsonFilePath;
        this.loadEmployeesFromJson();
    }

    private loadEmployeesFromJson(): void {
        try {
            const data = fs.readFileSync(this.jsonFilePath, 'utf-8');
            this.employees = JSON.parse(data);
        } catch (error) {
            console.error('Error while loading employees', error);
        }
    }
    private saveEmployeesToJson(): void {
        try {
            fs.writeFileSync(this.jsonFilePath, JSON.stringify(this.employees, null, 2), 'utf-8');
        } catch (error) {
            console.error('Error  while saving employees', error);
        }
    }

    addNewEmployee = async (employee: Employee): Promise<void> => {
        this.employees.push(employee);
        this.saveEmployeesToJson();
    }
    
    updateEmployeeInfo = async (id: number, updateEmployeeInfo: Employee): Promise<void> => {
        const specificEmployee = this.employees.findIndex(employee => employee.id === id);
        if (specificEmployee === -1) {
            throw new Error('Employee not found :(')
        }
        this.employees[specificEmployee] = updateEmployeeInfo;
        this.saveEmployeesToJson();
        
    }
    deleteEmployee = async (id: number): Promise<void> => {
        const specificEmployee = this.employees.findIndex(employee => employee.id === id);
        if (specificEmployee === -1) {
            throw new Error('The employee you are trying to delete does not exist :(')
        }
        this.employees.splice(specificEmployee, 1);
        this.saveEmployeesToJson();

    }
    
    findEmployeeById = async (id: number): Promise<Employee | undefined> => {
        return this.employees.find(employee => employee.id === id);
    }

}


const employeeManager = new EmployeeManager(jsonFilePath);

const internEmployee = createEmployee({
    id: 1,
    name: "Jane Smith",
    age: 22,
    role: "Intern",
    salary: 100,
    internshipDuration: "3 months",
    lead: "Lucky",
    vacationAvailability: false
} as Intern);


const managerEmployee = createEmployee({
    id: 2,
    name: "Jane doe",
    age: 28,
    role: "Manager",
    salary: 100,
    department: "HR",
    vacationAvailability: true,
} as Manager);


employeeManager.addNewEmployee(internEmployee);
employeeManager.addNewEmployee(managerEmployee);