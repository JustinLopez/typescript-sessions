// Ejercicio 1

type DataItemStore = number | string | object;

  class DataStore<T extends DataItemStore> {
    private store: T[] = [];
  
    addItem(item: T): void {
      this.store.push(item);
    }
  
    getItemByIndex(index: number): T | undefined {
      return this.store[index];
    }
  
    removeItemByIndex(index: number): T | undefined {
      return this.store.splice(index, 1)[0];
    }
  }
  
  const newObject = new DataStore<DataItemStore>();
  const newNumber = new DataStore<DataItemStore>();
  const newString = new DataStore<DataItemStore>();


  newObject.addItem({ id: 123, name: "Name", desc: "string" });
  newNumber.addItem(1234);
  newString.addItem('abcdefg');
  
  const storeWithDifferentTypes = new DataStore<{
    id: number;
    name: string;
    quantity: number;
  }>();
  storeWithDifferentTypes.addItem({id: 123, name: "Name", quantity: 12})


  console.log(newObject.getItemByIndex(0));
  console.log(newNumber);
  console.log(newString);


