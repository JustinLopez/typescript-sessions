// Ejercicio 2

interface Order {
    id: number;
    product: string;
    quantity: number;
    price: number;
}

interface PhysicalOrder extends Order {
    deliveryAddress: string;
}

interface VirtualOrder  extends Order {
    emailAddress: string;
}


class OrderManager<T extends Order> {
    private orders: T[];

    constructor() {
        this.orders = [];
    }

    addOrder(order: T): void {
        this.orders.push(order);
        console.log(`Order added: ${order.id}`);
    }

    getOrderDetails(orderId: number): T | undefined {
        const order = this.orders.find((order) => order.id === orderId);
        if (!order) {
            console.log(`Order with ID ${orderId} not found`);
            return undefined;
        }
        console.log(`Order details retrieved for order ID ${orderId}`);
        return order;
    }

    calculateTotalSales(): number {
        const totalSales = this.orders.reduce((total, order) => total + (order.quantity * order.price), 0);
        console.log(`Total sales calculated: $${totalSales.toFixed(2)}`);
        return totalSales;
    }
    settingDiscount(discount: number): void {
       const appliedDiscount = this.orders.forEach(order => {
            order.price -= order.price * discount;
        });
        console.log(`Applied discount: ${appliedDiscount}`);
        return appliedDiscount;
    }
    calculateTotalWDiscount(discount: number): number {
        this.settingDiscount(discount);
        return this.calculateTotalSales();
    }
}

//Physical

const physicalOrder = new OrderManager<PhysicalOrder>();
const physicalOrder1: PhysicalOrder = { id: 1, product: 'Nintendo Switch', quantity: 2, price: 500, deliveryAddress: 'random address' };


physicalOrder.addOrder(physicalOrder1);
physicalOrder.getOrderDetails(1);
physicalOrder.calculateTotalSales();
console.log("Venta con descuento: " + (physicalOrder.calculateTotalWDiscount(1.9)));


// Virtual
const virtualOrder = new OrderManager<VirtualOrder>();
const virtualOrder1: VirtualOrder = { id: 2, product: 'Laptop', quantity: 1, price: 1000, emailAddress: 'random@example.com' };

const order2: Order = { id: 2, product: 'Laptop', quantity: 1, price: 1000 };
virtualOrder.addOrder(virtualOrder1);
virtualOrder.getOrderDetails(2);
virtualOrder.calculateTotalSales();
console.log("Venta con descuento: " + (virtualOrder.calculateTotalWDiscount(1.9)));