 namespace Models {
    export class Item<T> {
        constructor(public name: string, public price: number, public data: T) {}
    }
}

export default Models;
