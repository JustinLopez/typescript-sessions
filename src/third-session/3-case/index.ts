import Utilities from './Utilities';
import Models from './Models';

const clampedValue = Utilities.clamp(15, 0, 10);
console.log("Clamped value:", clampedValue);

const shuffledArray = Utilities.shuffle([1, 2, 3, 4, 5]);
console.log("Shuffled array:", shuffledArray);

const item = new Models.Item<number>("Example Item", 10.99, 123);
console.log("Item:", item);
