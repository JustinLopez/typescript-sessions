function ValidateEmail(target: any, propertyKey: string) {
    let value: string;

    const getter = function () {
        return value;
    };

    const setter = function (newVal: string) {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (emailRegex.test(newVal)) {
            value = newVal;
        } else {
            throw new Error("Invalid email format");
        }
    };

    Object.defineProperty(target, propertyKey, {
        get: getter,
        set: setter
    });
}

function Required(target: any, propertyKey: string) {
    let value: any;

    const getter = function () {
        return value;
    };

    const setter = function (newVal: any) {
        if (newVal === undefined || newVal === null) {
            throw new Error(`${propertyKey} is required.`);
        }
        value = newVal;
    };

    Object.defineProperty(target, propertyKey, {
        get: getter,
        set: setter
    });
}

function ValidateType(expectedType: string) {
    return function (target: any, propertyKey: string) {
        let value: any;

        const getter = function () {
            return value;
        };

        const setter = function (newVal: any) {
             if (typeof newVal !== expectedType) {
                throw new Error(`Invalid type. Expected ${expectedType}`);
            }
            value = newVal;
        };

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter
        });
    }
}
class User {

    @Required
    @ValidateType("string")
    name: string;

    @Required
    @ValidateType("string")
    lastName: string;

    @Required
    @ValidateType("string")
    @ValidateEmail
    email: string;

    constructor(name: string, lastName: string, email: string) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }
    showUser() {
        console.log(`Name: ${this.name} Last Name: ${this.lastName}  email: ${this.email}`);
    }
}

let randomUser: User;
randomUser = new User('justin', 'lópez', 'whatever@exapmple.com');
randomUser.showUser();

try {
    randomUser.email = "imustapologize"; 
} catch (error: any) {
    console.log(error.message); 
}
try {
    // randomUser.lastName = false;  already throwing an error :(
} catch (error: any) {
    console.log(error.message); 
}


