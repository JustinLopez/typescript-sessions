import { promises as fs } from 'fs';

interface NoteData {
    title: string;
    description: string;
    userId: string;
    dueDate: string;
}

function validateType(type: string) {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];

        const getter = function () {
            return value;
        };

        const setter = function (newVal: any) {
            if (typeof newVal !== type) {
                throw new Error(`Invalid type. ${propertyKey} should be of type ${type}.`);
            }
            value = newVal;
        };

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true,
        });
    };
}
function validateLength(target: any, propertyKey: string) {
    let value = target[propertyKey];

    const getter = function () {
        return value;
    };

    const setter = function (newVal: string) {
        if (!newVal || newVal.length < 3) {
            throw new Error(`Title is required and should have at least 3 characters.`);
        }
        value = newVal;
    };

    Object.defineProperty(target, propertyKey, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true,
    });
}
class Note {
    @validateLength
    title: string;

    @validateType('string')
    description: string;

    @validateType('string')
    userId: string;

    @validateType('string')
    dueDate: string;

    constructor(title: string, description: string, userId: string, dueDate: string) {
        this.title = title;
        this.description = description;
        this.userId = userId;
        this.dueDate = dueDate;
    }

    static fromJSON(json: NoteData): Note {
        return new Note(json.title, json.description, json.userId, json.dueDate);
    }

    toJSON(): NoteData {
        return {
            title: this.title,
            description: this.description,
            userId: this.userId,
            dueDate: this.dueDate
        };
    }
}

class Log {
    timestamp: string;
    action: string;
    argumentsList?: any[] | null;
    deletedIndex?: number;
    updatedIndex?: number;

    constructor(timestamp: string, action: string, argumentsList?: any[], deletedIndex?: number, updatedIndex?: number) {
        this.timestamp = timestamp;
        this.action = action;
        this.argumentsList = argumentsList;
        this.deletedIndex = deletedIndex;
        this.updatedIndex = updatedIndex;

    }
}

// log decorator 
function actionLogger(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    let logs: Log[] = [];
    let notes: Note[] = [];

    const originalMethod = descriptor.value;
    descriptor.value = async function (...args: any[]) {
        const timestamp = new Date().toISOString();
        const action = propertyKey;
        const log: Log = { timestamp, action, argumentsList: args };
        const logData = await fs.readFile('logs.json', 'utf-8');

        if (logData) {
            logs = JSON.parse(logData);
        }

        if (action === 'create' || action === 'editNote') {
            const myNotes = await fs.readFile('mynotes.json', 'utf-8');
            if (myNotes) {
                notes = JSON.parse(myNotes);
            }
        }

        if (action === 'removeNote') {
            const deletedIndex = args[0];
            log.deletedIndex = deletedIndex + 1;
            const note = notes[deletedIndex];
            log.argumentsList = [{ ...note }];
        }

        if (action === 'editNote') {
            const updatedIndex = args[0];
            log.updatedIndex = updatedIndex + 1;
            const newNote = args[1];
            log.argumentsList = [{ ...newNote }];
        }


        logs.push(log);

        const result = await originalMethod.apply(this, args);

        const logsJSON = JSON.stringify(logs, null, 2);
        await fs.writeFile('logs.json', logsJSON);

        return result;
    };

    return descriptor;
}


class NotesApp {
    private filePath: string;

    constructor(filePath: string) {
        this.filePath = filePath;
    }

    @actionLogger
    async create(note: Note): Promise<void> {
        const notes = await this.read();
        notes.push(note.toJSON());
        await this.write(notes);
    }

    @actionLogger
    async read(): Promise<NoteData[]> {
        try {
            const data = await fs.readFile(this.filePath, 'utf-8');
            return JSON.parse(data);
        } catch (error) {
            return [];
        }
    }
    @actionLogger
    async write(notes: any[]): Promise<void> {
        const notesJSON = JSON.stringify(notes, null, 2);
        await fs.writeFile(this.filePath, notesJSON);
    }

    @actionLogger
    async editNote(index: number, updatedNote: Note): Promise<void> {
        const notes = await this.read();
        notes[index] = updatedNote.toJSON();
        await this.write(notes);
    }

    @actionLogger
    async removeNote(index: number): Promise<void> {
        const notes = await this.read();
        notes.splice(index, 1);
        await this.write(notes);
    }
}


// path and json with all my notes

const notesFilePath = 'mynotes.json';
const notesApp = new NotesApp(notesFilePath);


async function createNotes() {
    try {
        const firstNote = new Note('This is my first note', 'first note description', 'u-1', '2024-04-08');
        await notesApp.create(firstNote);
        console.log('Note 1 created successfully.');

        const secondNote = new Note('This is my second note', 'second note description', 'u-2', '2024-04-10');
        await notesApp.create(secondNote);
        console.log('Note 2 created successfully.');


        const thirdNote = new Note('This is third note', 'third note description', 'u-3', '2024-04-12');
        await notesApp.create(thirdNote);
        console.log('Note 3 created successfully.');

        const fourthNote = new Note('This is fourth note', 'third note description', 'u-4', '2024-04-12');
        await notesApp.create(fourthNote);
        console.log('Note 4 created successfully.');

    } catch (error: any) {
        console.error('Error creating notes:', error.message);
    }
}

// createNotes();

// editing notes

const editNote = new Note('This is my first edited note', 'first edited description', 'u-3', '2024-04-08');
notesApp.editNote(2, editNote)


// deleting notes 

// notesApp.removeNote(3);

// Crear una instancia de la clase Note
const note = new Note("Test", "Description", "user123", "2024-04-05");

try {
    // note.title = 123; already getting -> Type 'number' is not assignable to type 'string'. 
} catch (error: any) { 
    console.error(error.message); // Debería imprimir "Invalid type. title should be of type string."
}

try {
    note.title = ""; 
} catch (error: any) {
    console.error(error.message); 
}

try {
    note.title = "ab"; 
} catch (error: any) {
    console.error(error.message);
}
