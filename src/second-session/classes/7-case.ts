
// 7- Interfaces para tipos híbridos:
// a. Crea una interfaz "AdminUser" que combine propiedades de un usuario regular como "name" y "email" con propiedades de un administrador como "permissions" y "role".
// b. Define funciones que acepten objetos que cumplan con la interfaz "AdminUser" y muestren información combinada de usuario y administrador.
// c. Crea instancias de objetos que cumplan con la interfaz "AdminUser" y pasa los objetos a las funciones definidas para comprobar su comportamiento.


interface AdminUser {
    name: string;
    email: string;
    permissions: string[];
    role: string;
}

const showAdminUser = (adminUser: AdminUser): void => {
    console.log("Name ->", adminUser.name);
    console.log("Email ->", adminUser.email);
    console.log("Permissions ->", adminUser.permissions.join(", "));
    console.log("Rol ->", adminUser.role);
};

const firstAdminUser: AdminUser = {
    name: "Justin",
    email: "whatever@example.com",
    permissions: ["create", "read", "update", "delete"],
    role: "admin"
};

const secondAdminUser: AdminUser = {
    name: "Lucky",
    email: "lucky@example.com",
    permissions: ["read"],
    role: "moderator"
};

showAdminUser(firstAdminUser);
showAdminUser(secondAdminUser);