// 6- Composición:
// a. Crea una clase "Engine" con métodos como "start" y "stop".
// b. Utiliza la clase "Engine" como una propiedad de la clase "Car" para representar el motor del coche.
// c. Crea un objeto "Car" con un motor específico y llama a sus métodos para controlar el funcionamiento del coche.

class Engine {
    start(): void {
        console.log("The engine has started.");
    }

    stop(): void {
        console.log("The engine has stopped.");
    }
}

class Cars {
    engine: Engine;

    constructor(engine: Engine) {
        this.engine = engine;
    }


    startEngine(): void {
        this.engine.start();
    }

    stopEngine(): void {
        this.engine.stop();
    }
}
const carEngine = new Engine(); 
const myCar = new Cars(carEngine); 

myCar.startEngine();
myCar.stopEngine(); 
