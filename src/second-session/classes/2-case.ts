// 2- Abstracción de datos:
// a. Crea una clase abstracta "Shape" con métodos abstractos para calcular el área y el perímetro.
// b. Implementa la clase abstracta en clases concretas como "Circle" y "Rectangle", definiendo cómo se calculan sus áreas y perímetros.
// c. Crea objetos de las clases concretas y llama a los métodos para calcular áreas y perímetros de diferentes figuras.


abstract class Shape {
    abstract calculateArea(): number;
    abstract calculatePerimeter(): number;
}

class Circle extends Shape {
    private radius: number;

    constructor(radius: number) {
        super();
        this.radius = radius;
    }
    calculateArea(): number {
        return Math.PI * this.radius ** 2;
    }

    calculatePerimeter(): number {
        return 2 * Math.PI * this.radius;
    }
}

class Rectangle extends Shape {
    private width: number;
    private height: number;

    constructor(width: number, height: number) {
        super();
        this.width = width;
        this.height = height;
    }

    calculateArea(): number {
        return this.width * this.height;
    }

    calculatePerimeter(): number {
        return 2 * (this.width + this.height);
    }
}

const circle = new Circle(5);
console.log("Circle's area", circle.calculateArea());
console.log("Circle's perimeter", circle.calculatePerimeter());

console.log("-------------------------");

const rectangle = new Rectangle(4, 6);
console.log("Rectangle's area", rectangle.calculateArea());
console.log("Rectangle's perimeter:", rectangle.calculatePerimeter());
