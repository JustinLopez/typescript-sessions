// 4- Herencia:
// a. Crea una clase base "Animal" con propiedades y métodos como "name" y "eat".
// b. Crea clases hijas como "Dog" y "Cat" que hereden de "Animal" y añadan funcionalidades específicas como "bark" y "purr".
// c. Crea instancias de las clases hijas y prueba sus métodos heredados y propios.


class Animal {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    eat(food: string): void {
        console.log(`${this.name} is eating ${food}.`);
    }
}

class Doggo extends Animal {
    constructor(name: string) {
        super(name);
    }

    bark(): void {
        console.log(`${this.name} is barking.`);
    }
}

class Kitty extends Animal {
    constructor(name: string) {
        super(name);
    }

    purr(): void {
        console.log(`${this.name} is purring`);
    }
}

const myDog = new Doggo("Buddy");
myDog.eat("beef");
myDog.bark();

const myCat = new Kitty("Whiskers");
myCat.eat("tuna");
myCat.purr();