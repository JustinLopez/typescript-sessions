// 3- Encapsulación:
// a. Crea una clase "BankAccount" con propiedades privadas como "balance" y métodos públicos para depositar y retirar dinero.
// b. Implementa la encapsulación utilizando métodos públicos para acceder y modificar el saldo de la cuenta.
// c. Crea una instancia de "BankAccount" y realiza operaciones de depósito y retiro para probar la encapsulación.


class BankAccount {
    private balance: number;

    constructor(initialBalance: number) {
        this.balance = initialBalance;
    }


    deposit(amount: number): void {
        if (amount <= 0) {
            console.log("The deposit amount must be greater than 0.");
            return;
        }
        
        this.balance += amount;
        console.log(`$${amount} was deposited. New balance: $${this.balance}`);
    }


    withdraw(amount: number): void {
        if (amount <= 0) {
            console.log("The withdrawal amount must be greater than 0.");
            return;
        }

        if (amount > this.balance) {
            console.log("Insufficient funds to make the withdrawal.");
            return;
        }

        this.balance -= amount;
        console.log(`$${amount} was withdrawn. New balance: $${this.balance}`);
    }

    getBalance(): number {
        return this.balance;
    }
}

const myAccount = new BankAccount(1000);

console.log("Initial balance:", myAccount.getBalance());
myAccount.deposit(500);
myAccount.withdraw(200);
myAccount.withdraw(1500);