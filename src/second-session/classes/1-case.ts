
// 1- Modelado de objetos:
// a. Crea una clase "Car" con propiedades como "brand", "model" y "price".
// b. Define métodos para mostrar información detallada del coche y calcular el precio de venta con impuestos.
// c. Crea instancias de la clase "Car" y utiliza sus métodos para obtener información y precios de diferentes modelos.

class Car {
    brand: string;
    model: string;
    price: number;

    constructor(brand: string, model: string, price: number) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }
    showDetails(): void {
        console.log(`Brand ->: ${this.brand}`);
        console.log(`Model -> ${this.model}`);
        console.log(`Price -> $${this.price}`);
    }

    calculateSalePrice(): number {
        const taxRate = 0.1;
        const salePrice = this.price * (1 + taxRate);
        return salePrice;
    }
}

const firstCar = new Car("Toyota", "Corolla", 25000);
const secondCar = new Car("Honda", "Civic", 27000);

firstCar.showDetails();
console.log("For sale price (with taxes):", firstCar.calculateSalePrice());

console.log("-------------------------");

secondCar.showDetails();
console.log("For sale price (with taxes):", secondCar.calculateSalePrice());
