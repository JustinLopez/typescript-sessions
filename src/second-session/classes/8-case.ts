// 8- Componentes reutilizables:
// a. Crea una clase "Button" con propiedades y métodos para representar un botón en una interfaz de usuario.
// b. Utiliza la clase "Button" en múltiples lugares de una aplicación para mostrar botones reutilizables con diferentes estilos y funciones.
// c. Modifica la clase "Button" para agregar nuevas funcionalidades y asegúrate de que los cambios se reflejen en todos los botones utilizados en la aplicación.

class Button {
    private text: string;
    private style: string;
    private disabled: boolean;

    constructor(text: string, style: string) {
        this.text = text;
        this.style = style;
        this.disabled = false;
    }

    render(): void {
        console.log(`[${this.style}] ${this.text}${this.disabled ? " (Disabled)" : ""}`);
    }

    changeStyle(newStyle: string): void {
        this.style = newStyle;
    }

    setDisabled(disabled: boolean): void {
        this.disabled = disabled;
    }
}

const firstBtn = new Button("Send", "primary");
const secondBtn = new Button("Cancel", "secondary");

firstBtn.render(); 
secondBtn.render();

const button3 = new Button("Save", "Success");
button3.render();
button3.setDisabled(true); 
button3.render();
