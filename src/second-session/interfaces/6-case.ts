// 6- Interfaces indexables:
// a. Crea una interfaz "Dictionary" que permita acceder a sus elementos mediante claves de tipo string y devuelva valores de tipo string.
// b. Implementa la interfaz "Dictionary" en una clase y agrega métodos para añadir elementos, obtener un elemento por clave y mostrar todos los elementos del diccionario.
// c. Crea una instancia del diccionario, añade elementos y prueba los métodos de acceso y visualización.



interface Dictionary {
    [key: string]: string;
}

class StringDictionary {
    private items: Dictionary;

    constructor() {
        this.items = {};
    }

    add(key: string, data: string): void {
        this.items[key] = data;
    }

    get(key: string): string | undefined {
        return this.items[key];
    }

    showAll(): void {
        for (const key in this.items) {
            console.log(`Key: ${key}, Value: ${this.items[key]}`);
        }
    }
}

const dictionary = new StringDictionary();

dictionary.add("apple", "manzana");
dictionary.add("banana", "plátano");
dictionary.add("cherry", "cereza");
console.log('----------- Obtener un elemento por clave -----------');
console.log(dictionary.get("banana"));
console.log('----------- Mostrar todos los elementos del diccionario -----------');
dictionary.showAll();