// 3- Extensión de interfaces:
// a. Crea una interfaz base "Employee" con propiedades como "name" y "surname".
// b. Extiende la interfaz "Employee" en dos nuevas interfaces, "Developer" y "Designer", añadiendo propiedades específicas para cada tipo de empleado.
// c. Crea objetos de tipo "Developer" y "Designer" y accede a sus propiedades para verificar la extensión de la interfaz.

interface Employee {
    name: string;
    surname: string;
}

interface Developer extends Employee {
    technologies: string[];
}

interface Designer extends Employee {
    designTools: string[];
}

const developer: Developer = {
    name: "Lucky",
    surname: "SMBS",
    technologies:  ["nextJS", "React", "WordPress"]
};

const designer: Designer = {
    name: "Bluey",
    surname: "SMBS",
    designTools: ["Figma", "Adobe XD", "Sketch"]
};

console.log("Developer:");
console.log("Name:", developer.name);
console.log("Surname:", developer.surname);
console.log("Technologies:", developer.technologies);

console.log("Designer:");
console.log("Name:", designer.name);
console.log("Surname", designer.surname);
console.log("Design Tools", designer.designTools);
