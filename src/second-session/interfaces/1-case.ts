// 1- Propiedades opcionales:
// a. Crea una interfaz "User" con propiedades obligatorias como "name" y "surname", y una propiedad opcional como "email".
// b. Define una función que acepte un objeto que cumpla con la interfaz "User" y muestre un mensaje personalizado según si el "email" está presente o no.
// c. Crea dos instancias de objetos "User", una con "email" y otra sin "email", y pásalas a la función definida para verificar su comportamiento.


interface User {
    name: string;
    surname: string;
    phoneNumber?: string;
}

const showMessage = (user: User): void => {
    if (user.phoneNumber) {
        console.log(`Hi ${user.name} ${user.surname}. Your phone number is ${user.phoneNumber}.`);
        return;
    }

    console.log(`Hi ${user.name} ${user.surname}. You have'nt provided a phone number.`);
};

const user1: User = { name: "Justin", surname: "López", phoneNumber: "123456789" };
const user2: User = { name: "Dulce", surname: "Lorente" };

showMessage(user1); 
showMessage(user2);
