// 7- Implementa el patrón Singleton en una clase "Logger" para gestionar registros de eventos de forma única.
// b. Utiliza el patrón Factory Method para crear instancias de diferentes tipos de vehículos en una fábrica de vehículos.
// c. Aplica el patrón Observer para notificar a diferentes clases cuando se produzca un cambio en un objeto de datos.


class Logger {
    private static instance: Logger;
    private log: string[];

    private constructor() {
        this.log = [];
    }

    public static getInstance(): Logger {
        if (!Logger.instance) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    }

    public logEvent(event: string): void {
        this.log.push(event);
        console.log(`Event logged: ${event}`);
    }

    public getLog(): string[] {
        return this.log;
    }
}

const firstLogger = Logger.getInstance();
firstLogger.logEvent("User logged in");

const secondLogger = Logger.getInstance();
secondLogger.logEvent("User logged out");

console.log(firstLogger === secondLogger); 
console.log(firstLogger.getLog()); 

interface Vehicle {
    drive(): void;
}

class Car implements Vehicle {
    drive(): void {
        console.log("Driving a car...");
    }
}

class Bike implements Vehicle {
    drive(): void {
        console.log("Riding a bike...");
    }
}


class VehicleFactory {
    private vehicleTypes: { [key: string]: () => Vehicle } = {
        "car": () => new Car(),
        "bike": () => new Bike()
    };

    createVehicle(type: string): Vehicle {
        const createFn = this.vehicleTypes[type];
        if (!createFn) {
            throw new Error("Invalid vehicle type :(");
        }
        return createFn();
    }
}

const factory = new VehicleFactory();

const car = factory.createVehicle("car");
car.drive(); 

const bike = factory.createVehicle("bike");
bike.drive(); 


interface Observer {
    update(data: any): void;
}

class DataSource {
    private observers: Observer[] = [];

    addObserver(observer: Observer): void {
        this.observers.push(observer);
    }

    notifyObservers(data: any): void {
        this.observers.forEach(observer => observer.update(data));
    }
}

class DataObserver implements Observer {
    update(data: any): void {
        console.log(`Received data update: ${data}`);
    }
}

const dataSource = new DataSource();
const firstObserver = new DataObserver();
const secondObserver = new DataObserver();

dataSource.addObserver(firstObserver);
dataSource.addObserver(secondObserver);

dataSource.notifyObservers("New data available");