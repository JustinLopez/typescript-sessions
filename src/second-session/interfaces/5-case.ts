// 5- Interfaces para funciones:
// a. Crea una interfaz "Calculator" con un método "sum" que acepte dos números y devuelva su suma.
// b. Implementa la interfaz "Calculator" en una función y prueba su funcionamiento llamándola con diferentes números.
// c. Define otra función que acepte un objeto que cumpla con la interfaz "Calculator" y use su método "sum" para realizar una operación.

interface Calculator {
    sum(firstNumber: number, secondNumber: number): number;
}

const calculatorFunction: Calculator = {
    sum(firstNumber: number, secondNumber: number): number {
        return firstNumber + secondNumber;
    }
};

console.log(calculatorFunction.sum(5, 3)); // Output: 8
console.log(calculatorFunction.sum(10, -2)); // Output: 8


function performCalculation(calculator: Calculator, firstNumber: number, secondNumber: number): number {
    return calculator.sum(firstNumber, secondNumber);
}

console.log(performCalculation(calculatorFunction, 10, 20));

