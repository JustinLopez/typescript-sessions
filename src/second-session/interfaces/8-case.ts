interface Container<T> {
    add(item: T): void;
    get(index: number): T | undefined;
    getAll(): T[];
}

class GenericContainer<T> implements Container<T> {
    private items: T[] = [];

    add(item: T): void {
        this.items.push(item);
    }

    get(index: number): T | undefined {
        return this.items[index];
    }

    getAll(): T[] {
        return this.items;
    }
}

const numberContainer = new GenericContainer<number>();
numberContainer.add(1);
numberContainer.add(2);
numberContainer.add(3);
console.log(numberContainer.getAll()); 
console.log(numberContainer.get(1)); 

const stringContainer = new GenericContainer<string>();
stringContainer.add("hello");
stringContainer.add("world");
console.log(stringContainer.getAll()); 
console.log(stringContainer.get(0)); 

interface Person {
    name: string;
    age: number;
}

const objectContainer = new GenericContainer<Person>();
objectContainer.add({ name: "Alice", age: 30 });
objectContainer.add({ name: "Bob", age: 25 });
console.log(objectContainer.getAll()); 
console.log(objectContainer.get(1)); 
